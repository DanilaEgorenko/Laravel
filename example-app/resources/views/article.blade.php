@include ('templates.header')
<body class="w-75 mx-auto">
    <h1 class="d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">{{$article->name}}</h1>
    @if($article->full_image)
        <img class="rounded mx-auto d-block h-50" src="{{$article->full_image_src}}">
    @endif
    <p class="font-weight-light text-center p-3">{{$article->desc}}</p>

    <h4>Комментарии</h4>
    <ul>
    @foreach($article->comments as $comment)
        <li><b>{{$comment->author}}</b> <i>{{$comment->text}}</i></li>
    @endforeach
    </ul>

    <div>
    <form method="POST" action="/articles/{{$article->id}}/comments">
        @csrf
        <div>
            <label>Имя: <input name="author"/></label>
        </div>
        <div>
        <label>Комментарий: <textarea name="text"></textarea></label>
        </div>
        <input type="submit" value="Create">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form>
</div>
</body>
@include ('templates.footer')