@include ('templates.header')
<body>
    <h1 class="d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">Новости</h1>
    <div class="news list-group d-grid gap-2 w-auto">
        @foreach($articles as $article) 
            <div class="newsItem list-group-item rounded-3 py-3">
                <a href="/articles/{{$article->id}}">{{$article->name}}</a>
                <p>{{$article->shortDesc}}</p>
            </div>
        @endforeach
    </div>
</body>
@include ('templates.footer')