<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Article;
use \App\Models\Comment;

class ArticleController extends Controller
{
    public function index() {
        $articles = Article::all();
        return view('articles', ['articles'=>$articles]);
    }

    public function get($articleId) {
        $article = Article::findOrFail($articleId);
        return view('article', ['article'=>$article]);
    }

    public function storeComment($articleId, Request $request) {
        try {
            $validated = $request->validate(
                [
                    'author' => 'required | min:2',
                    'text' => 'required'
                ]
                );
                $article = Article::findOrFail($articleId);
                $newComment = new Comment();
                $newComment->fill($validated);
                $newComment->article()->associate($article);
                $newComment->save();

                return redirect()->back();
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors($exception->validator);
        }
    }
}
