<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\ArticleController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/articles', [ArticleController::class, 'index']);

Route::get('/articles/{articleId}', [ArticleController::class, 'get'])->name('article');

Route::post('/articles/{articleId}/comments', [ArticleController::class, 'storeComment']);